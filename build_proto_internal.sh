python -m grpc.tools.protoc\
 --proto_path=./proto/maum/common\
 --python_out=./proto/maum/common\
 --grpc_python_out=./proto/maum/common\
 lang.proto
python -m grpc.tools.protoc\
 --proto_path=./proto\
 --python_out=./proto\
 --grpc_python_out=./proto\
 ng_tts.proto
python -m grpc.tools.protoc\
 --proto_path=./proto\
 --python_out=./proto\
 --grpc_python_out=./proto\
 brain_wav2lip.proto