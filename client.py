# -*- coding: utf-8 -*-
import grpc
import argparse
import os
import time
from multiprocessing import Queue, Process


import sys
sys.path.insert(0, "./proto")
from proto.brain_lipsync_pb2_grpc import LipSyncGenerationStub
import proto.brain_lipsync_pb2 as pb2

CHUNK_SIZE = 1024 * 1024

class LipSyncClient(object):
    def __init__(self, remote):
        channel = grpc.insecure_channel(remote)
        self.stub = LipSyncGenerationStub(channel)
        self.chunk_size = CHUNK_SIZE
        os.makedirs("output_file", exist_ok=True)

    def determinate_and_save(self, text, background_binary, transparent, resolution):
        if transparent:
            output_filename = os.path.join("output_file", "lipsync_client_output.webm")
        else:
            output_filename = os.path.join("output_file", "lipsync_client_output.mp4")

        video_binary = self._determinate(text, background_binary, transparent, resolution)
        self._save_chunks_to_file(video_binary, output_filename)

        return output_filename

    def _determinate(self, text, background_binary, transparent, resolution):
        resolution = self._get_tuple_resolution(resolution)
        lip_sync_iterator = self._generate_binary_iterator(text, background_binary, transparent, resolution)
        key = self.stub.Upload(lip_sync_iterator)

        print(key.request_key)
        waiting_key = key.request_key
        status_value = pb2.NOT_YET
        while status_value != pb2.DONE:
            time.sleep(1)
            status = self.stub.StatusCheck(pb2.LipSyncRequestKey(request_key=waiting_key))
            if status.status_code == pb2.NOT_YET:
                waiting_num = status.waiting
                print("{:s} ... {:d} people are waiting.".format(status.msg, waiting_num))
            else:
                print("{:s}".format(status.msg))
            status_value = status.status_code

        output = self.stub.Download(pb2.LipSyncRequestKey(request_key=waiting_key))

        video_binary = self._save_chunks_to_binary(output)

        return bytes(video_binary)

    @staticmethod
    def _generate_binary_iterator(text, background, transparent, resolution):
        resolution = pb2.VideoResolution(width=resolution[0], height=resolution[1])
        yield pb2.LipSyncInput(text=text, resolution=resolution, transparent=transparent)

        if background is not None:
            for idx in range(0, len(background), CHUNK_SIZE):
                yield pb2.LipSyncInput(background=background[idx:idx+CHUNK_SIZE])

    @staticmethod
    def _save_chunks_to_file(chunks, filename):
        print("Saved file in client_tmp -> " + filename)
        with open(filename, 'wb') as f:
            f.write(chunks)

    @staticmethod
    def _save_chunks_to_binary(chunks):
        binary = bytearray()
        for chunk in chunks:
            binary.extend(chunk.video)

        return binary
    
    @staticmethod
    def _get_tuple_resolution(resolution):
        if isinstance(resolution, str):
            if resolution == "FHD":
                resolution = (1920, 1080)
            elif resolution == "HD":
                resolution = (1280, 720)
            elif resolution == "SD":
                resolution = (640, 360)
            else:
                raise AssertionError("resolution should be either FHD, HD, or SD.")
        else:
            pass
        return resolution

def run_test(remote, result_queue, image_path, text, transparent, resolution):
    client = LipSyncClient(remote)

    if image_path is None:
        background_data = None
    else:
        with open(image_path, 'rb') as rf:
            background_data = rf.read()

    tic = time.time()
    result = client.determinate_and_save(text, background_data, transparent, resolution)
    toc = time.time()

    result_queue.put(toc - tic)


def print_result(result_queue, cpu_num):
    start_time = time.perf_counter()
    results = []
    runtimes = []
    while len(runtimes) < cpu_num:
        results.append(result_queue.get())
        runtime = time.perf_counter() - start_time
        runtimes.append(runtime)

    print("Average runtime for {:d} scales: {:.04f}".format(cpu_num, sum(results) / cpu_num))

def main():
    parser = argparse.ArgumentParser(
        description='Client Example for Lip Sync Generator (work with Wav2Lip and TTS)')
    parser.add_argument('--ip', nargs='?', dest='ip', default='127.0.0.1',
                        help='grpc: ip', type=str)
    parser.add_argument('--port', nargs='?', dest='port', default=45001,
                        help='grpc: rank 0 port', type=int)
    parser.add_argument('-t', '--text',  default='안녕하세요? 마인즈랩입니다.',
                        help='text for speech synthesis', type=str)
    parser.add_argument('-i', '--image_path', nargs='?',
                        help='image file path', type=str)
    parser.add_argument('-m', '--media_format', default="mp4",
                        help='image file path', type=str, choices=["mp4", "webm"])
    parser.add_argument('--width', default=0,
                        help='width for video', type=int)
    parser.add_argument('--height', default=0,
                        help='height for video', type=int)
    parser.add_argument('--resolution', default="FHD", choices=["SD", "HD", "FHD"],
                        help='Options for well-used resolutions. If width and height are given, it will be ignored.', type=str)
    parser.add_argument('--n_cpu', nargs='?', dest='n_cpu', default=10,
                        help='n_cpu', type=int)
    parser.add_argument('--use_scale',
                        help='use mps with scaled containers', action="store_true")
    args = parser.parse_args()

    print(args.width, args.height)
    if args.width > 0 and args.height > 0:
        resolution = (args.width, args.height)
    else:
        resolution = args.resolution

    args.transparent = (args.media_format == "webm")

    result_queue = Queue()
    if args.use_scale:
        test_list = [Process(target=run_test,
                             args=('{}:{}'.format(args.ip, args.port + cpu_idx), result_queue,
                                   args.image_path, args.text, args.transparent, resolution))
                     for cpu_idx in range(0, args.n_cpu)]
    else:
        test_list = [Process(target=run_test,
                             args=('{}:{}'.format(args.ip, args.port), result_queue,
                                   args.image_path, args.text, args.transparent, resolution))
                     for _ in range(0, args.n_cpu)]
    for test in test_list:
        test.start()

    print_result(result_queue, args.n_cpu)


if __name__ == '__main__':
    main()
