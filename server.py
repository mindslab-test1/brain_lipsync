# -*- coding: utf-8 -*-
import logging
import argparse
import grpc
import time
import os
from datetime import datetime
import uuid

from concurrent import futures

import sys
sys.path.insert(0, "./proto")
from proto.brain_lipsync_pb2_grpc import add_LipSyncGenerationServicer_to_server, LipSyncGenerationServicer
import proto.brain_lipsync_pb2 as pb2

from wav2lip_client import Wav2LipGenerationClient
from ng_tts_client import NgTtsClient
from util_server.schedule_dict import get_time_difference, ScheduleDict

_ONE_DAY_IN_SECONDS = 60 * 60 * 24
_CHUNK_SIZE = 1024 * 1024
_REST_TIMEOUT = 60 * 10  # 600 secs
_CONNECTION_LOST = 15  # 15 secs which user should ask the status if he/she is waiting
_FMT = '%Y-%m-%d %H:%M:%S'

scheduler = ScheduleDict()


class LipSyncRunner:
    def __init__(self, tts_port, wav2lip_port):
        self.tts_port = tts_port
        self.wav2lip_port = wav2lip_port
        self.logger = logging.getLogger('lip-sync')

    def _get_tts_result(self, input_text):
        tts_client = NgTtsClient(self.tts_port)
        try:
            audio_binary = tts_client.determinate(input_text)
            tts_client.close_channel()
            return audio_binary
        except:
            tts_client.close_channel()
            return None

    def _get_wav2lip_result(self, audio_binary, background_binary, transparent, resolution):
        wav2lip_client = Wav2LipGenerationClient(self.wav2lip_port)
        try:
            if len(background_binary) == 0:
                background_binary = None
            wav2lip_result_iterator =\
                wav2lip_client.determinate(audio_binary, background_binary, transparent, resolution)
            if wav2lip_result_iterator is None:
                raise RuntimeError("the iterator from wav2lip has nothing.")

            video_binary = self._get_video_binary(wav2lip_result_iterator)

            wav2lip_client.close_channel()
            return video_binary
        except:
            wav2lip_client.close_channel()
            return None

    def run(self):
        self.logger.debug(scheduler)

        try:
            self._clean_scheduler()
        except RuntimeError as e:
            self.logger.error("{:s}".format(str(e)))
        except KeyError as e:
            self.logger.error("{:s}".format(str(e)))

        for _key, _elem in scheduler:
            # REST TIMEOUT이 되지 않은 Job들인데 아직 진행되지 않은 걸 발견했을 때
            if _elem["status"] is pb2.NOT_YET:
                strftime_now = datetime.fromtimestamp(time.time()).strftime(_FMT)
                time_after_asked = get_time_difference(strftime_now, _elem["asked_at"])

                if time_after_asked > _CONNECTION_LOST:  # connection lost
                    scheduler.put_element(_key, status=pb2.DELETED, done_at=strftime_now)
                    continue

                scheduler.put_element(_key, status=pb2.PROCESSING)
                self.logger.info("For now, doing " + _key)
                text, background_binary, speaker_id, transparent, resolution =\
                    scheduler.get_element(_key, 'text', 'background', 'speaker_id', 'transparent', 'resolution')
                try:
                    # speaker_id is not used now.
                    video_binary = self._process(text, background_binary, transparent, resolution)
                    scheduler.put_element(_key, video=video_binary)
                    strftime_now = datetime.fromtimestamp(time.time()).strftime(_FMT)
                    scheduler.put_element(_key, status=pb2.DONE, done_at=strftime_now)

                except RuntimeError as e:
                    self.logger.error("For {:s}, \n{:s}".format(_key, str(e)))
                    strftime_now = datetime.fromtimestamp(time.time()).strftime(_FMT)
                    scheduler.put_element(_key, status=pb2.ERROR, done_at=strftime_now,
                                          error_msg=e)
                except KeyError as e:
                    self.logger.error("For {:s}, \n{:s}".format(_key, str(e)))

    def _process(self, input_text, background_binary, transparent, resolution):

        audio_binary = self._get_tts_result(input_text)
        if audio_binary is None:
            raise RuntimeError("it is impossible to generate audio with the given text.")

        video_binary = self._get_wav2lip_result(audio_binary, background_binary, transparent, resolution)

        if video_binary is None:
            raise RuntimeError("video is not generated.")

        return video_binary

    @staticmethod
    def _clean_scheduler():
        strftime_now = datetime.fromtimestamp(time.time()).strftime(_FMT)

        remove_keys = set()
        timeout_keys = []
        abnormal_status_keys = []
        for _key, _elem in scheduler:
            waiting_time = get_time_difference(strftime_now, _elem["create_at"])
            _status = _elem["status"]
            if waiting_time > _REST_TIMEOUT:
                timeout_keys.append(_key)
            if _status == pb2.DELETED:
                abnormal_status_keys.append(_key)

        remove_keys.update(timeout_keys)
        remove_keys.update(abnormal_status_keys)

        for _key in remove_keys:
            scheduler.remove_element(_key, return_val=False)

    @staticmethod
    def _get_video_binary(wav2lip_iterator):
        video_binary = bytearray()
        for chunk in wav2lip_iterator:
            if chunk.video is not None:
                video_binary.extend(chunk.video)

        video_binary = bytes(video_binary)
        return video_binary


class LipSyncGenerationServicerImpl(LipSyncGenerationServicer):
    def __init__(self, server_temp_dir="server_tmp"):
        self.temp_dir = server_temp_dir
        os.makedirs(self.temp_dir, exist_ok=True)
        self.logger = logging.getLogger('lip-sync')

    def Upload(self, request_iterator, context):
        try:
            input_text = ""
            speaker_id = 0
            transparent = False
            width = 0
            height = 0           
            background_data = bytearray()
            for input_data in request_iterator:
                if input_data.text is not None and len(input_data.text) != 0:
                    input_text = input_data.text
                if input_data.speaker_id is not None and input_data.speaker_id != 0:
                    speaker_id = input_data.speaker_id
                if input_data.background is not None and len(input_data.background) != 0:
                    background_data.extend(input_data.background)
                if input_data.transparent is not None and input_data.transparent:
                    transparent = input_data.transparent
                if input_data.resolution is not None:
                    if input_data.resolution.width != 0:
                        width = input_data.resolution.width
                    if input_data.resolution.height != 0:
                        height = input_data.resolution.height

            request_key = str(uuid.uuid4())
            background_data = bytes(background_data)

            if not isinstance(input_text, str):
                raise AssertionError("Text is not string.")
            
            if width > 0 and height > 0:
                resolution = (width, height)
            else:
                raise AssertionError("Both width and height should be bigger than 0.")


            strftime_now = datetime.fromtimestamp(time.time()).strftime(_FMT)
            scheduler.add_element(request_key, create_at=strftime_now, asked_at=strftime_now,
                                  text=input_text, background=background_data,
                                  speaker_id=speaker_id, transparent=transparent, resolution=resolution)
            return pb2.LipSyncRequestKey(request_key=request_key)
        except Exception as e:
            self.logger.exception(e)
            context.set_code(grpc.StatusCode.FAILED_PRECONDITION)
            context.set_details(str(e))

    def StatusCheck(self, request, context):
        try:
            strftime_now = datetime.fromtimestamp(time.time()).strftime(_FMT)
            if not scheduler.is_exist(request.request_key):
                return pb2.StatusMsg(status_code=pb2.WRONG_KEY, msg="Wrong Key")

            scheduler.put_element(request.request_key, asked_at=strftime_now)
            _status_val = scheduler.get_element(request.request_key, 'status', 'order')
            if _status_val is None:
                return pb2.StatusMsg(status_code=pb2.DELETED, msg="Deleted")

            else:  # job caught and check with job status
                status, order = _status_val[0], _status_val[1]
                if status is pb2.NOT_YET:
                    return pb2.StatusMsg(status_code=pb2.NOT_YET, msg="Not yet", waiting=order)
                elif status == pb2.DONE:
                    return pb2.StatusMsg(status_code=pb2.DONE, msg="Finished")
                elif status == pb2.ERROR:
                    return pb2.StatusMsg(status_code=pb2.ERROR, msg="Process Error")
                elif status == pb2.PROCESSING:
                    return pb2.StatusMsg(status_code=pb2.PROCESSING, msg="Now Processing")
                elif status == pb2.DELETED:
                    return pb2.StatusMsg(status_code=pb2.DELETED, msg="Deleted by connection lost")
                else:
                    return pb2.StatusMsg(status_code=pb2.ERROR,
                                         msg="File key exists, but it seems to have an error in Upload.")

        except Exception as e:
            self.logger.exception(e)
            context.set_code(grpc.StatusCode.UNKNOWN)
            context.set_details(str(e))
        except KeyError as e:
            self.logger.exception("By Client: key error when status check... {:s}".format(str(e)))
            return pb2.StatusMsg(status_code=pb2.WRONG_KEY, msg="Wrong Key")

    def Download(self, request, context):
        try:

            video_binary, status = scheduler.get_element(request.request_key, 'video', 'status')
            if status == pb2.DONE:
                output_iterator = self._generate_lipsync_iterator(video_binary)
                return output_iterator
            elif status == pb2.NOT_YET:
                self.logger.exception(
                    "Client tries to download the video of {:s}"
                    "though it has been not yet started.".format(request.request_key))
                context.set_code(grpc.StatusCode.UNAVAILABLE)
                context.set_details("Not yet started. You may wait for a while.")
            elif status == pb2.PROCESSING:
                self.logger.exception("Job {:s} is not finished but client try to download it.".format(request.request_key))
                context.set_code(grpc.StatusCode.UNAVAILABLE)
                context.set_details("{:s} has not been finished yet.".format(request.request_key))
            elif status == pb2.ERROR:
                error_msg = scheduler.get_element(request.request_key, 'error_msg')[0]
                self.logger.exception("Job {:s} has an error but"
                                      "client tries to download. Error: {:s}".format(request.request_key, error_msg))
                context.set_code(grpc.StatusCode.FAILED_PRECONDITION)
                context.set_details("During processing, the error occurs as {:s}".format(str(error_msg)))
            elif status == pb2.DELETED:
                self.logger.exception(
                    "Client tries to download the video of {:s} though connection lost.".format(request.request_key))
                context.set_code(grpc.StatusCode.DEADLINE_EXCEEDED)
                context.set_details("Connection was lost.")

        except RuntimeError as e:
            self.logger.exception(str(e))
            context.set_code(grpc.StatusCode.UNKNOWN)
            context.set_details(str(e))
        except KeyError as e:
            self.logger.exception("By Client: key error when downloading... {:s}".format(str(e)))
            context.set_code(grpc.StatusCode.UNKNOWN)
            context.set_details("Key error when downloading. \n{:s}".format(str(e)))

    @staticmethod
    def _generate_lipsync_iterator(video_binary):
        for idx in range(0, len(video_binary), _CHUNK_SIZE):
            yield pb2.LipSyncResult(video=video_binary[idx:idx+_CHUNK_SIZE])


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Lip Sync Generator (work with Wav2Lip and TTS)')
    parser.add_argument("--port", type=int, default=45001, help='grpc port for lip sync servicer')
    parser.add_argument('--tts_remote',
                        help='tts grpc: ip:port',
                        type=str,
                        default='127.0.0.1:9999')
    parser.add_argument('--wav2lip_remote',
                        help='wav2lip grpc: ip:port',
                        type=str,
                        default='127.0.0.1:45101')
    parser.add_argument('--log_level', help='logger level', type=str, default='INFO')
    args = parser.parse_args()

    logging.basicConfig(
        level=getattr(logging, args.log_level),
        format='[%(levelname)s|%(filename)s:%(lineno)s][%(asctime)s] >>> %(message)s'
    )

    os.makedirs("logs", exist_ok=True)

    lipsync_logger = logging.getLogger('lip-sync')
    file_handler = logging.FileHandler('./logs/log_server.log')
    lipsync_logger.addHandler(file_handler)

    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10), )
    lip_sync_servicer = LipSyncGenerationServicerImpl()
    add_LipSyncGenerationServicer_to_server(lip_sync_servicer, server)
    server.add_insecure_port('[::]:{}'.format(args.port))
    server.start()

    runner = LipSyncRunner(args.tts_remote, args.wav2lip_remote)
    lipsync_logger.info('LipSyncGeneration starting at 0.0.0.0:%d', args.port)
    try:
        while True:
            runner.run()
            time.sleep(1)
    except KeyboardInterrupt:
        server.stop(0)
