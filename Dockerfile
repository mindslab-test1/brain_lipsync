FROM docker.maum.ai:443/brain/vision:cu101

COPY . /root/lipsync
WORKDIR /root/lipsync
RUN python -m pip --no-cache-dir install -r requirements.txt
RUN python -m pip uninstall -y protobuf
RUN python -m pip --no-cache-dir install --no-binary=protobuf protobuf
ENV LANG=ko_KR.UTF-8
EXPOSE 45001

RUN bash build_proto_internal.sh
RUN bash build_proto_external.sh

ENV PORT 45001
ENV TTS_REMOTE 127.0.0.1:9999
ENV WAV2LIP_REMOTE 127.0.0.1:45101
ENV LOG_LEVEL INFO

ENTRYPOINT python server.py --port $PORT --tts_remote $TTS_REMOTE --wav2lip_remote $WAV2LIP_REMOTE --log_level $LOG_LEVEL