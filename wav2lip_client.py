import grpc
import argparse
import os

import sys
sys.path.insert(0, "./proto")
from proto.brain_wav2lip_pb2_grpc import Wav2LipGenerationStub
from proto.brain_wav2lip_pb2 import Wav2LipInput, Wav2LipResult, VideoResolution

CHUNK_SIZE = 1024 * 1024

class Wav2LipGenerationClient(object):
    def __init__(self, remote):
        self.channel = grpc.insecure_channel(remote)
        self.stub = Wav2LipGenerationStub(self.channel)

    def determinate(self, audio_binary, background_binary, transparent, resolution):
        return self._determinate(audio_binary, background_binary, transparent, resolution)
    
    def determinate_and_save(self, audio_binary, background_binary, transparent, resolution):
        video_iterator = self._determinate(audio_binary, background_binary, transparent, resolution)
        
        if transparent:
            output_filename = os.path.join("output_file", "sample_output.webm")
        else:
            output_filename = os.path.join("output_file", "sample_output.mp4")
        os.makedirs(os.path.dirname(output_filename), exist_ok=True)
        self._save_chunks_to_file(video_iterator, output_filename)

        return output_filename
    
    def close_channel(self):
        self.channel.close()
        
    def _determinate(self, audio_binary, background_binary, transparent, resolution):
        resolution = self._get_tuple_resolution(resolution)
        binary_iterator = self._generate_binary_iterator(audio_binary, background_binary, transparent, resolution)
        video_iterator = self.stub.Determinate(binary_iterator)
        return video_iterator

    @staticmethod
    def _generate_binary_iterator(audio_binary, background_binary, transparent, resolution):
        yield Wav2LipInput(transparent=transparent, resolution=VideoResolution(width=resolution[0], height=resolution[1]))
        
        for idx in range(0, len(audio_binary), CHUNK_SIZE):
            yield Wav2LipInput(audio=audio_binary[idx:idx + CHUNK_SIZE])

        if background_binary is not None:
            for idx in range(0, len(background_binary), CHUNK_SIZE):
                yield Wav2LipInput(background=background_binary[idx:idx + CHUNK_SIZE])

    @staticmethod
    def _save_chunks_to_file(chunks, filename):
        with open(filename, 'wb') as f:
            for chunk in chunks:
                f.write(chunk.video)

    @staticmethod
    def _get_tuple_resolution(resolution):
        if isinstance(resolution, str):
            if resolution == "FHD":
                resolution = (1920, 1080)
            elif resolution == "HD":
                resolution = (1280, 720)
            elif resolution == "SD":
                resolution = (640, 360)
            else:
                raise AssertionError("resolution should be either FHD, HD, or SD.")
        else:
            pass
        return resolution


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='gRPC client code for wav2lip engine')
    parser.add_argument('-r', '--remote',
                        nargs='?',
                        dest='remote',
                        help='gRPC options (format: ip:port)',
                        type=str,
                        default='127.0.0.1:45002')
    parser.add_argument('-a', '--audio_path',
                        nargs='?',
                        help='Audio file path',
                        type=str,
                        required=True)
    parser.add_argument('-i', '--image_path',
                        nargs='?',
                        help='Image file path',
                        type=str,
                        required=True)
    parser.add_argument('-f', '--format',
                        default="mp4",
                        choices=["mp4", "webm"],
                        help='Video format (mp4, webm supported)',
                        type=str)
    parser.add_argument('--width',
                        default=0,
                        help='width for video',
                        type=int)
    parser.add_argument('--height',
                        default=0,
                        help='height for video',
                        type=int)
    parser.add_argument('--resolution',
                        default="FHD",
                        choices=["SD", "HD", "FHD"],
                        help='Options for well-used resolutions. If width and height are given, it will be ignored.',
                        type=str)

    args = parser.parse_args()

    format_list = ['mp4', 'webm']
    if not args.format.lower() in format_list:
        raise AssertionError(f'args.format should be one of {format_list}, currently given "{args.format.lower()}')

    transparent = args.format.lower() == "webm"

    if args.width > 0 and args.height > 0:
        resolution = (args.width, args.height)
    else:
        resolution = args.resolution

    client = Wav2LipGenerationClient(args.remote)

    with open(args.audio_path, 'rb') as rf:
        audio_data = rf.read()

    with open(args.image_path, 'rb') as rf:
        background_data = rf.read()

    result = client.determinate_and_save(audio_data, background_data, transparent, resolution)  # thanks to python, resolution can be either str or tuple.
