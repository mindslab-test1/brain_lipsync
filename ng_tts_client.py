import grpc
import argparse
import os

import sys
sys.path.insert(0, "./proto")
from proto.ng_tts_pb2_grpc import NgTtsServiceStub
from proto.ng_tts_pb2 import TtsRequest

CHUNK_SIZE = 1024 * 1024
SPEAKER_ID = 0  # TODO: if multi speakers in tts servicer, it has to be a variable.


class NgTtsClient(object):
    def __init__(self, remote='127.0.0.1:11000', chunk_size=1024):
        self.channel = grpc.insecure_channel(remote)
        self.stub = NgTtsServiceStub(self.channel)
        self.chunk_size = chunk_size

    def determinate(self, text):
        output = self.stub.SpeakWav(TtsRequest(text=text, speaker=SPEAKER_ID))
        audio_binary = self._save_chunks_to_binary(output)

        return bytes(audio_binary)

    def determinate_and_save(self, text):
        self.output_filename = os.path.join("output_file", "sample_output.wav")
        os.makedirs(os.path.dirname(self.output_filename), exist_ok=True)
        audio_binary = self.determinate(text)
        self._save_chunks_to_file(audio_binary, self.output_filename)

        return self.output_filename

    def close_channel(self):
        self.channel.close()

    @staticmethod
    def _save_chunks_to_file(chunks, filename):
        print("Saved file in client_tmp -> " + filename)
        with open(filename, 'wb') as f:
            f.write(chunks)

    @staticmethod
    def _save_chunks_to_binary(chunks):
        audio_binary = bytearray()
        for chunk in chunks:
            audio_binary.extend(chunk.mediaData)

        return audio_binary


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='id card authentication client')
    parser.add_argument('-r', '--remote',
                        nargs='?',
                        dest='remote',
                        help='grpc: ip:port',
                        type=str,
                        default='182.162.19.30:9999')
    parser.add_argument('-t', '--text',
                        help='text for speech synthesis',
                        type=str,
                        default='안녕하세요? 마인즈랩입니다.')
    args = parser.parse_args()

    client = NgTtsClient(args.remote)
    print(args.text)
    result = client.determinate_and_save(args.text)
