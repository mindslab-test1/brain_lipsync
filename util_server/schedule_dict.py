import os
import time
import csv
import logging
from datetime import datetime
import threading

from proto.brain_lipsync_pb2 import NOT_YET, PROCESSING, DONE, ERROR, DELETED

logger = logging.getLogger(name='lip-sync')
job_class_lock = threading.RLock()


def _generate_csv(db_filepath):
    if not os.path.isfile(db_filepath):
        with open(db_filepath, 'w', newline='') as process_db:
            wr = csv.writer(process_db)
            wr.writerow(["KEY", "create_at", "done_at", "status", "text", "transparent", "resolution"])


def _append_csv(db_filepath, job_key, job_elem):
    with open(db_filepath, 'a', newline='') as process_db:
        wr = csv.writer(process_db)
        wr.writerow([job_key, job_elem["create_at"], job_elem["done_at"], job_elem["status"], job_elem["text"],
                     str(job_elem["transparent"]), job_elem["resolution"]])


def get_time_difference(strftime_now, strftime_old):
    FMT = '%Y-%m-%d %H:%M:%S'
    tdelta = datetime.strptime(strftime_now, FMT) - datetime.strptime(strftime_old, FMT)
    return tdelta.total_seconds()


class ScheduleDict:
    def __init__(self):
        self.job_scheduler = dict()
        self.order = 0
        os.makedirs("logs", exist_ok=True)
        self.db_path = "./logs/request_log.csv"
        _generate_csv(self.db_path)

    def add_element(self, request_key, return_val=True, **kwargs):
        with job_class_lock:
            self.job_scheduler[request_key] = {"created_at": None, "done_at": None, "status": NOT_YET, "text": None,
                                               "asked_at": None, "background": None, "speaker_id": None,
                                               "transparent": None, "resolution": None, "video": None,
                                               "order": self.order, "error_msg": None}
            self.order += 1

            try:
                for key, value in kwargs.items():
                    self.job_scheduler[request_key][key] = value
            except KeyError as e:
                raise e

            if return_val:
                return self.job_scheduler[request_key]

    def get_element(self, request_key, *args):
        with job_class_lock:
            try:
                job_elem = self.job_scheduler[request_key]
            except KeyError:
                return None

            if args is None:
                return job_elem
            else:
                return_val = [job_elem[key] for key in args]
                return return_val

    def remove_element(self, request_key, return_val=True):
        with job_class_lock:
            try:
                popped_elem = self.job_scheduler.pop(request_key)
                _append_csv(self.db_path, request_key, popped_elem)  # no need to lock in addition
            except KeyError as e:
                raise e

            if return_val:
                return popped_elem

    def put_element(self, request_key, **kwargs):
        waiting_order_update = False
        previous_status = [PROCESSING, NOT_YET]
        now_status = [DELETED, DONE, ERROR]
        with job_class_lock:
            try:
                for key, value in kwargs.items():
                    if key == 'status':
                        if self.job_scheduler[request_key][key] in previous_status and value in now_status:
                            waiting_order_update = True
                    self.job_scheduler[request_key][key] = value
            except KeyError as e:
                raise e

        if waiting_order_update:
            key_list = self._get_key_list()
            with job_class_lock:
                self.order -= 1  # deduct 1 from the number of waiting people
                for key in key_list:
                    try:
                        self.job_scheduler[key]["order"] -= 1  # notice all other jobs that someone is finished
                    except KeyError:
                        pass

    def is_exist(self, request_key):
        with job_class_lock:
            try:
                job_elem = self.job_scheduler[request_key]
                if job_elem:
                    return True
                else:
                    return False
            except KeyError:
                return False

    def _get_key_list(self):
        with job_class_lock:
            return list(self.job_scheduler.keys())

    def __iter__(self):
        with job_class_lock:
            return iter(list(self.job_scheduler.items()))

    def __str__(self):
        with job_class_lock:
            if len(self.job_scheduler) != 0:
                return "\t".join(["{:s}...{:d}".format(key, status) for key, status in
                                  zip(self.job_scheduler.keys(), [x['status'] for x in self.job_scheduler.values()])])
            else:
                return "No Process"

    def __len__(self):
        with job_class_lock:
            return len(self.job_scheduler)
